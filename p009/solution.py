#!/usr/bin/env python3
from lib.numerical import isqrt


def main():
    for a in range(3, 500):
        b_numerator = 1000 * (500 - a)
        b_denominator = 1000 - a
        if not b_numerator % b_denominator:
            b = b_numerator // b_denominator
            n = a * a + b * b
            c = isqrt(n)
            if c * c == n:
                return a * b * c


# >>> main()
# 31875000
