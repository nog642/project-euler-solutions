#!/usr/bin/env python3
from lib.numerical import primes


def main(n):
    s = 0
    for p in primes():
        if p >= n:
            break
        s += p
    return s


# >>> main(2000000)
# 142913828922
