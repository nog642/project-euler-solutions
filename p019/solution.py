#!/usr/bin/env python3
DAYS = ('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun')


def leap_year(year):
    return year % 4 == 0 and (year % 100 != 0 or year % 400 == 0)


def month_length(month, year):
    if month in (9, 4, 6, 11):
        return 30
    if month == 2:
        return 29 if leap_year(year) else 28
    return 31


def dow(year, month, day):
    days = sum(366 if leap_year(y) else 365 for y in range(1900, year))
    days += sum(month_length(m, year) for m in range(1, month))
    days += day
    return DAYS[days % 7]


def main():
    sundays = 0
    for year in range(1901, 2001):
        for month in range(1, 13):
            if dow(year, month, 1) == "Sun":
                sundays += 1
    return sundays


# >>> main()
# 171
