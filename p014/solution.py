#!/usr/bin/env python3


def main(limit):
    max_seq_len = 0
    longest_n = 0
    for n in range(1, limit):
        seq_len = 0
        k = n
        while k != 1:
            if k % 2:
                k = 3 * k + 1
            else:
                k //= 2
            seq_len += 1
        if seq_len > max_seq_len:
            max_seq_len = seq_len
            longest_n = n
    return longest_n


# >>> main(1000000)
# 837799
