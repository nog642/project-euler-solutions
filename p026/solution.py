#!/usr/bin/env python3
from fractions import Fraction
import itertools
from math import floor


def decimalize(num, den):
    if num > den:
        integer_part = str(num // den)
        num %= den
    else:
        integer_part = ''
    fractional_part = ''
    repeating_part = ''

    n = Fraction(num, den)
    while True:
        if not n.numerator:
            break

        if n.denominator % 2 and n.denominator % 5:
            x = 10
            for k in itertools.count(1):
                if x % n.denominator == 1:
                    repeating_part = '{:0{}}'.format(
                        n.numerator * ((x - 1) // n.denominator),
                        k
                    )
                    break
                x *= 10
            break

        n *= 10
        fractional_part += str(floor(n))
        n %= 1

    return integer_part, fractional_part, repeating_part


def represent(integer_part, fractional_part, repeating_part):
    retval = integer_part
    if fractional_part or repeating_part:
        retval += '.'
    if fractional_part:
        retval += fractional_part
    if repeating_part:
        retval += '({})'.format(repeating_part)
    return retval


def main(n):
    if n <= 2:
        raise ValueError('n is too low')
    max_cycle_length = -1
    max_cycle_d = None
    for d in range(2, n):
        integer_part, fractional_part, repeating_part = decimalize(1, d)
        # print('1/{}\t{}'.format(d, represent(integer_part, fractional_part,
        #                                      repeating_part)))
        cycle_length = len(repeating_part)
        if cycle_length > max_cycle_length:
            max_cycle_length = cycle_length
            max_cycle_d = d
    return max_cycle_d


# >>> main(1000)
# 983
