#!/usr/bin/env python3
from lib.numerical import divisors


def d(n):
    return sum(divisors(n))


def main(m):
    a = set()
    for n in range(2, m):
        dn = d(n)
        if n != dn and d(dn) == n:
            a.add(n)
    return sum(a)


# >>> main(10000)
# 31626
