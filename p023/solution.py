#!/usr/bin/env python3
from lib.numerical import divisors


def abundant(n):
    return sum(divisors(n)) > n


def abundant_pairable(n):
    # print('ap {}'.format(n))
    for a in range(1, n // 2 + 1):
        if abundant(a) and abundant(n - a):
            return True
    return False


def main():
    return sum(n for n in range(1, 28123) if not abundant_pairable(n))


# >>> main()
# 4179871
