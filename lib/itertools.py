#!/usr/bin/env python3
from collections import deque
from itertools import chain, combinations, islice


def consume(iterator, n=None):
    """
    Advance the iterator n-steps ahead. If n is None, consume entirely.
    :param iterator:
    :param n:
    """
    # Use functions that consume iterators at C speed.
    if n is None:
        # feed the entire iterator into a zero-length deque
        deque(iterator, maxlen=0)
    else:
        # advance to the empty slice starting at position n
        next(islice(iterator, n, n), None)


def window(iterable, n):
    """
    Returns an iterator over windows of size n over iterable.

    Windows are in the form of a mutable deque. Do not mutate the deque. If
    output must be stored, use map(tuple, window(iterable, n)).

    :param iterable: Iterable to window over.
    :param n: Window size.
    :return: Iterator over windows.
    """
    it = iter(iterable)
    win = deque(islice(it, n), n)
    if len(win) < n:
        return
    yield win
    append = win.append
    for e in it:
        append(e)
        yield win


def ilen(iterable):
    """
    Count the number of items that `iterable` yields.
    """
    d = deque(enumerate(iterable, 1), maxlen=1)
    return d[0][0] if d else 0


def all_combinations(iterable):
    sequence = tuple(iterable)
    return chain(*(combinations(sequence, r) for r in range(len(sequence) + 1)))
