#!/usr/bin/env python3
from functools import reduce
from math import gcd
from operator import mul

_KNOWN_PRIMES = (2, 3, 5, 7)
_PRIMALITY_MODS = (1, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61,
                   67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 121,
                   127, 131, 137, 139, 143, 149, 151, 157, 163, 167, 169, 173,
                   179, 181, 187, 191, 193, 197, 199, 209)
_NUM_PRIMALITY_MODS = len(_PRIMALITY_MODS)


def divisors(n):
    """
    :param n: integer >= 1 (behavior undefined if n < 1)
    :return: iterator over proper divisors of n in no particular order
    """
    if n == 1:
        return
    yield 1
    i = 2
    while i * i < n:
        if not n % i:
            yield i
            yield n // i
        i += 1
    if i * i == n:
        yield i


def factorize(n):
    i = 0
    while i < 4:
        if not n % _KNOWN_PRIMES[i]:
            yield _KNOWN_PRIMES[i]
            n //= _KNOWN_PRIMES[i]
            continue
        i += 1
    k = 0
    while (k + 1)**2 <= n:
        i = 0 if k else 1
        while i < _NUM_PRIMALITY_MODS:
            r = k + _PRIMALITY_MODS[i]
            if not n % r:
                yield r
                n //= r
                continue
            i += 1
        k += 210
    if n > 1:
        yield n


def fibonacci():
    a = 1
    b = 1
    while True:
        yield a
        yield b
        a += b
        b += a


def isqrt(n):
    if n <= 0:
        if not n:
            return 0
        raise ValueError("square root is only defined for nonnegative numbers")

    x = 1 << (n.bit_length() + 1 >> 1)
    while True:
        y = x + n // x >> 1
        if y >= x:
            return x
        x = y


def lcm(*args):
    l = len(args)
    if l == 2:
        a, b = args
        return a * b // gcd(a, b)
    if l > 2:
        return lcm(args[0], lcm(*args[1:]))
    if l:
        return args[0]
    return 1


def primes():
    yield 2
    prev = []
    i = 1
    while True:
        i += 2
        cont = False
        for p in prev:
            if not i % p:
                cont = True
                break
            if p * p >= i:
                break
        if cont:
            continue
        yield i
        prev.append(i)


def prod(iterable):
    return reduce(mul, iterable, 1)
