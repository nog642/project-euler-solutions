#!/usr/bin/env python3
from math import factorial


def nperm(n, elems):
    elems = list(elems)
    k = len(elems)
    val = factorial(k)
    if n >= val:
        raise ValueError('n ({}) is too large, as there are not that many '
                         'permutations of elems ({})'.format(n, elems))
    retval = [None] * k
    for d in range(k):
        val //= k - d
        i = n // val
        retval[d] = elems[i]
        del elems[i]
        n %= val
    return tuple(retval)


def main(n):
    return ''.join(map(str, nperm(n - 1, range(10))))


# >>> main(1000000)
# '2783915460'
