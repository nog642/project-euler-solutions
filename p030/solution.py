#!/usr/bin/env python3


def maxdigits(n):
    """
    Finds an upper bound on the number of digits in a number x such that the
    sum of the nth powers of the digits of x is equal to x.
    :param n:
    :return: The largest x such that x * 9^n >= 10^(x - 1).
    """
    if n < 1:
        raise ValueError
    # (10 * 9^n)x - 10^x = 0
    c = 10 * 9**n
    x0 = 1
    x2 = n + 1
    if c * x2 > 10**x2:
        return x2
    while x0 < x2:
        x1 = x2
        x2 = x1 // 10 * 9  # arbitrary decrease by ~90%
        if c * x2 > 10**x2:
            x0 = x2
            break
    else:
        raise AssertionError
    while True:
        x2 = (x0 + x1) // 2
        if x2 == x0:
            return x0
        fx2 = c * x2 - 10**x2
        if fx2 > 0:
            x0 = x2
        else:
            x1 = x2


def solutions(exponent):
    for n in range(2, sum(10**i * 9 for i in range(maxdigits(exponent)))):
        if sum(int(d)**exponent for d in str(n)) == n:
            yield n


def main(exponent):
    return sum(solutions(exponent))


# >>> main(5)
# 443839
