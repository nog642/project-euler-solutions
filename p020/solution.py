#!/usr/bin/env python3
from math import factorial


def digit_sum(n):
    return sum(map(int, str(n)))


def main(n):
    return digit_sum(factorial(n))


# >>> main(100)
# 648
