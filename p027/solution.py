#!/usr/bin/env python3
import itertools

KNOWN_PRIMES = (2, 3, 5)
PRIMALITY_MODS = (1, 7, 11, 13, 17, 19, 23, 29)


def is_prime(n):
    if n < 2:
        return False
    if n in KNOWN_PRIMES:
        return True
    for p in KNOWN_PRIMES:
        if not n % p:
            return False
    k = 0
    while (k + 1)**2 <= n:
        for i in PRIMALITY_MODS[0 if k else 1:]:
            r = k + i
            if not n % r:
                return n == r
        k += 30
    return True


def main():
    maximum = -1
    for a in range(-999, 1000):
        # print(a)
        for b in range(-1000, 1001):
            f = lambda n: n**2 + a*n + b

            for n in itertools.count():
                if not is_prime(f(n)):
                    break

            if n > maximum:
                maximum = n
                max_a = a
                max_b = b
    # print((max_a, max_b))
    return max_a * max_b


# >>> main()
# -59231
