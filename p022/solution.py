#!/usr/bin/env python3
import json
import string

VALUES = {string.ascii_uppercase[i]: i + 1 for i in range(26)}


def load_names(fpath):
    with open(fpath) as f:
        data = f.read()
    return json.loads('[{}]'.format(data))


def value(name):
    return sum(VALUES[c] for c in name)


def main(names_fpath):
    names = sorted(load_names(names_fpath))
    return sum(value(name) * i for i, name in enumerate(names, 1))


# >>> main('p022_names.txt')
# 870873746
