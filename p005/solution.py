#!/usr/bin/env python3
from lib.numerical import lcm


def main(n):
    return lcm(*range(1, n + 1))


# >>> main(20)
# 232792560
