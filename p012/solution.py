#!/usr/bin/env python3
import itertools
from lib.numerical import divisors


def main(d):
    n = 0
    for i in itertools.count(1):
        n += i
        if len(tuple(divisors(n))) + 1 > d:
            return n


# >>> main(500)
# 76576500
