#!/usr/bin/env python3
from lib.numerical import factorize


def main(x):
    return max(factorize(x))


# >>> main(600851475143)
# 6857
