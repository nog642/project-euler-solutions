#!/usr/bin/env python3


def main(sidelength):
    if not sidelength % 2:
        raise ValueError('sidelength must be odd')
    skip = 2
    total = 1
    n = 1
    side = 1
    while side < sidelength:
        for _ in range(4):
            n += skip
            total += n
        skip += 2
        side += 2
    return total


# >>> main(1001)
# 669171001
