#!/usr/bin/env python3
import itertools


def main(arange, brange):
    return len(set(a**b for a, b in itertools.product(arange, brange)))


# >>> main(range(2, 101), range(2, 101))
# 9183
