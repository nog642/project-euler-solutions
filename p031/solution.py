#!/usr/bin/env python3
from functools import lru_cache


@lru_cache(maxsize=None)
def count_ways(parts, total):
    """
    Works best if parts is sorted in decreasing order.
    :param parts:
    :param total:
    :return:
    """
    if len(parts) == 1:
        return not total % parts[0]
    count = 0
    for i, part in enumerate(parts):
        if part < total:
            count += count_ways(parts[i:], total - part)
        elif part == total:
            count += 1
    return count


def main():
    return count_ways((200, 100, 50, 20, 10, 5, 2, 1), 200)


# >>> main()
# 73682
