#!/usr/bin/env python3
from fractions import Fraction

from lib.numerical import prod


def get_fractions():
    for numerator in range(10, 100):
        for denominator in range(numerator + 1, 100):

            numerator_str = str(numerator)
            denominator_str = str(denominator)
            for cancel_digit in '123456789':
                if cancel_digit in numerator_str and cancel_digit in denominator_str:
                    break
            else:
                continue

            canceled_numerator_list = list(numerator_str)
            canceled_numerator_list.remove(cancel_digit)
            canceled_numerator = int(''.join(canceled_numerator_list))

            canceled_denominator_list = list(denominator_str)
            canceled_denominator_list.remove(cancel_digit)
            canceled_denominator = int(''.join(canceled_denominator_list))

            if not canceled_denominator:
                # canceled fraction would be division by 0
                continue
            fraction = Fraction(numerator, denominator)
            if fraction == Fraction(canceled_numerator, canceled_denominator):
                yield fraction


def main():
    return prod(get_fractions())


# >>> main()
# Fraction(1, 100)
