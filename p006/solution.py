#!/usr/bin/env python3


def main(n):
    double_sum = n * (n + 1)
    return (double_sum // 2)**2 - double_sum * (2 * n + 1) // 6


# >>> main(100)
# 25164150
