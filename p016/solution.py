#!/usr/bin/env python3


def main(n):
    return sum(int(d) for d in str(2**n))


# >>> main(1000)
# 1366
