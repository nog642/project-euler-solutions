#!/usr/bin/env python3
from lib.numerical import primes
from lib.itertools import consume


def main(n):
    gen = primes()
    consume(gen, n - 1)
    return next(gen)


# >>> main(10001)
# 104743
