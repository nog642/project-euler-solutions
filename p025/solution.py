#!/usr/bin/env python3
from lib.numerical import fibonacci


def digits(n):
    return len(str(n))


def main(n):
    for i, f in enumerate(fibonacci(), 1):
        if digits(f) >= n:
            return i


# >>> main(1000)
# 4782
