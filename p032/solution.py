#!/usr/bin/env python3
from collections import defaultdict

from lib.numerical import divisors


def get_digit_combos(ndigits):
    for multiplicand_digits in range(1, ndigits):
        for multiplier_digits in range(multiplicand_digits, ndigits):
            product_digits = ndigits - multiplicand_digits - multiplier_digits
            if (multiplicand_digits - 1) + (multiplier_digits - 1) + 1 <= product_digits <= multiplicand_digits + multiplier_digits:
                yield product_digits, multiplicand_digits, multiplier_digits


def get_products(digits):
    digits = ''.join(sorted(digits))
    digit_combos = defaultdict(dict)
    for product_digits, multiplicand_digits, multiplier_digits in get_digit_combos(len(digits)):
        digit_combos[product_digits][multiplicand_digits] = multiplier_digits
    for product_digits, factor_digit_combos in digit_combos.items():
        for product in range(10**(product_digits - 1), 10**product_digits):
            for multiplicand in divisors(product):
                multiplicand_str = str(multiplicand)
                multiplicand_digits = len(multiplicand_str)
                if multiplicand_digits not in factor_digit_combos:
                    continue
                multiplier_str = str(product // multiplicand)
                if len(multiplier_str) != factor_digit_combos[multiplicand_digits]:
                    continue
                if ''.join(sorted(multiplicand_str + multiplier_str + str(product))) == digits:
                    # print("{} * {} = {}".format(multiplicand_str, multiplier_str, product))
                    yield product
                    break


def main():
    return sum(get_products('123456789'))


# >>> main()
# 593577
