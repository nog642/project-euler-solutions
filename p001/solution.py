#!/usr/bin/env python3


def main():
    total = 0
    for i in range(1000):
        if i % 3 == 0 or i % 5 == 0:
            total += i
    return total


# >>> main()
# 233168
