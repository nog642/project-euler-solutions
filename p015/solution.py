#!/usr/bin/env python3
from math import factorial


def main(n):
    over = factorial(n)
    return factorial(2 * n) // over // over


# >>> main(20)
# 137846528820
