#!/usr/bin/env python3
from lib.numerical import fibonacci


def main(maximum):
    fib = fibonacci()
    n = next(fib)
    total = 0
    while n < maximum:
        if not n % 2:
            total += n
        n = next(fib)
    return total


# >>> main(4000000)
# 4613732
